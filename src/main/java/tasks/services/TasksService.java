package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tasks.model.validator.DateValidator;
import tasks.model.validator.ValidatorException;
import tasks.repo.ArrayTaskList;
import tasks.model.Task;
import tasks.model.TasksOperations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TasksService {

    private ArrayTaskList tasks;

    public TasksService(ArrayTaskList tasks){
        this.tasks = tasks;
    }


    public ObservableList<Task> getObservableList(){
        return FXCollections.observableArrayList(tasks.getAll());
    }
    public String getIntervalInHours(Task task){
        int seconds = task.getRepeatInterval();
        int minutes = seconds / DateService.SECONDS_IN_MINUTE;
        int hours = minutes / DateService.MINUTES_IN_HOUR;
        minutes = minutes % DateService.MINUTES_IN_HOUR;
        return formTimeUnit(hours) + ":" + formTimeUnit(minutes);//hh:MM
    }
    public String formTimeUnit(int timeUnit){
        StringBuilder sb = new StringBuilder();
        if (timeUnit < 10) sb.append("0");
        if (timeUnit == 0) sb.append("0");
        else {
            sb.append(timeUnit);
        }
        return sb.toString();
    }

    public int parseFromStringToSeconds(String stringTime){//hh:MM
        String[] units = stringTime.split(":");
        int hours = Integer.parseInt(units[0]);
        int minutes = Integer.parseInt(units[1]);
        int result = (hours * DateService.MINUTES_IN_HOUR + minutes) * DateService.SECONDS_IN_MINUTE;
        return result;
    }

    public Iterable<Task> filterTasks(Date start, Date end) throws ValidatorException {
        DateValidator Valid = new DateValidator();
        Valid.validate(start,end);
        TasksOperations tasksOps = new TasksOperations(getObservableList());
        Iterable<Task> filtered = tasksOps.incoming(start,end);
        //Iterable<Task> filtered = tasks.incoming(start, end);
        return filtered;
    }
    /*
        private static Date fDate = new GregorianCalendar(1999, Calendar.DECEMBER, 31).getTime();
        private static Date lDate = new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime();

        public Iterable<Task> filterTasks2(Date start, Date end) throws ValidatorException {
            DateValidator Valid = new DateValidator();
            Valid.validate(start,end);

            if (fDate.compareTo(start) * start.compareTo(lDate)<=0){
                throw new ValidatorException("Date must be from 01.01.2000 to 31.12.2025");
            }
            if (fDate.compareTo(end) * end.compareTo(lDate)<=0){
                throw new ValidatorException("Date must be from 01.01.2000 to 31.12.2025");
            }
            if (start.compareTo(end)>=0){
                throw new ValidatorException("Start date should be smaller than end date");
            }

            ArrayList<Task> incomingTasks = new ArrayList<>();
            for (Task t : tasks) {
                Date nextTime = t.nextTimeAfter(start);
                if (nextTime != null && (nextTime.before(end) || nextTime.equals(end))) {
                    incomingTasks.add(t);
                    System.out.println(t.getTitle());
                }
            }

            return incomingTasks;
        }
   */
}
