package tasks.model.validator;

public class ValidatorException extends Exception{

    public ValidatorException() {
    }

    public ValidatorException(String message) {
        super(message);
    }
}
