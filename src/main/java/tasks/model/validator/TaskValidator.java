package tasks.model.validator;

import tasks.model.Task;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TaskValidator {

    private static Date fDate = new GregorianCalendar(1999, Calendar.DECEMBER, 31).getTime();
    private static Date lDate = new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime();

    public void validate (Task task) throws ValidatorException{
        if (task.getTitle().isEmpty()){
            throw new ValidatorException("Title must be between 1 and 255 characters");
        }

        if (fDate.compareTo(task.getStartTime()) * task.getStartTime().compareTo(lDate)<=0){
            throw new ValidatorException("Date must be from 01.01.2000 to 31.12.2025");
        }

    }

}
