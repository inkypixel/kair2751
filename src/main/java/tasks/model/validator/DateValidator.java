package tasks.model.validator;

import tasks.model.Task;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateValidator {

    private static Date fDate = new GregorianCalendar(1999, Calendar.DECEMBER, 31).getTime();
    private static Date lDate = new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime();

    public void validate (Date dates,Date datef) throws ValidatorException{
        if (fDate.compareTo(dates) * dates.compareTo(lDate)<=0){
            throw new ValidatorException("Date must be from 01.01.2000 to 31.12.2025");
        }
        if (fDate.compareTo(datef) * datef.compareTo(lDate)<=0){
            throw new ValidatorException("Date must be from 01.01.2000 to 31.12.2025");
        }
        if (dates.compareTo(datef)>=0){
            throw new ValidatorException("Start date should be smaller than end date");
        }
    }
}
