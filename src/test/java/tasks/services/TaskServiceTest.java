package tasks.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.Task;
import tasks.model.validator.ValidatorException;
import tasks.repo.ArrayTaskList;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TaskServiceTest {
    ArrayTaskList repo;
    TasksService serv;

    private static Date vDate = new GregorianCalendar(2000, Calendar.JANUARY, 1).getTime();
    private static Date tDate = new GregorianCalendar(2000, Calendar.JANUARY, 10).getTime();
    private static Date vDate2 = new GregorianCalendar(2000, Calendar.JANUARY, 20).getTime();
    private static Date ivDate = new GregorianCalendar(2026, Calendar.DECEMBER, 31).getTime();

    @BeforeEach
    void setup(){
        repo = new ArrayTaskList();
        serv = new TasksService(repo);
    }

    @Test
    void testPath1() {
        ValidatorException exception = assertThrows(ValidatorException.class, () -> {
            serv.filterTasks(ivDate,vDate2);
        });

        String expectedMessage = "Date must be from 01.01.2000 to 31.12.2025";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    void testPath2() {
        ValidatorException exception = assertThrows(ValidatorException.class, () -> {
            serv.filterTasks(vDate,ivDate);
        });

        String expectedMessage = "Date must be from 01.01.2000 to 31.12.2025";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    void testPath3() {
        ValidatorException exception = assertThrows(ValidatorException.class, () -> {
            serv.filterTasks(vDate2,vDate);
        });

        String expectedMessage = "Start date should be smaller than end date";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    void testPath4() {
        try {
            Iterable<Task> ls = serv.filterTasks(vDate,vDate2);
            for(Task task : ls){
                assertTrue(false);
                return;
            }
            assertTrue(true);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }
    @Test
    void testPath5(){
        Task ts = new Task("Test","descriprion", tDate, tDate, tDate, 0, true);
        try {
            repo.add(ts);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
        try {
            Iterable<Task> ls = serv.filterTasks(vDate,vDate2);
            for(Task task : ls){
                assertTrue(true);
                return;
            }
            assertTrue(false);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }
    @Test
    void testPath6() {
        try {
            Iterable<Task> ls = serv.filterTasks(vDate,vDate2);
            for(Task task : ls){
                assertTrue(false);
                return;
            }
            assertTrue(true);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }
}
