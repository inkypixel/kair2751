package tasks.repo;

import org.junit.jupiter.api.*;
import tasks.model.Task;
import tasks.model.validator.ValidatorException;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Tests for Tasks")
class ArrayTaskListTest {

    private Task task;
    private ArrayTaskList repo;
    private static Date fDate = new GregorianCalendar(2000, Calendar.JANUARY, 1).getTime();
    private static Date lDate = new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime();



    @BeforeEach
    void setUp() {
        repo = new ArrayTaskList();
    }

    //ECP

    @Test
    void testAddTaskTitleValidDateValid() {
        this.task = new Task("abc", new GregorianCalendar(2020, Calendar.FEBRUARY, 11).getTime());

        try {
            repo.add(task);
            assertTrue(repo.size() == 1);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAddTaskTitleEmptyDateInvalid(){
        this.task = new Task("", new GregorianCalendar(2026, Calendar.FEBRUARY, 11).getTime());

        ValidatorException exception = assertThrows(ValidatorException.class, () -> {
            repo.add(task);
        });

        String expectedMessage = "Title must be between 1 and 255 characters";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @RepeatedTest(3)
    void testAddTaskTitleValidDateInvalid() {
        this.task = new Task("new task", new GregorianCalendar(1999, Calendar.FEBRUARY, 11).getTime());

        ValidatorException exception = assertThrows(ValidatorException.class, () -> {
            repo.add(task);
        });

        String expectedMessage = "Date must be from 01.01.2000 to 31.12.2025";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));

    }

    //BVA

    @Test
    void testAddTaskTitleValidDateMinValid(){
        this.task = new Task("new task", new GregorianCalendar(2000, Calendar.JANUARY, 1).getTime());

        try {
            repo.add(task);
            assertTrue(repo.size()==1);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAddTaskTitleValidDateMaxInvalid() {
        // assertThrows
        this.task = new Task("new task", new GregorianCalendar(2026, Calendar.JANUARY, 1).getTime());

        ValidatorException exception = assertThrows(ValidatorException.class, () -> {
            repo.add(task);
        });

        String expectedMessage = "Date must be from 01.01.2000 to 31.12.2025";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void testAddTaskTitleInvalidDateMaxValid() {
        this.task = new Task("", new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime());
        ValidatorException exception = assertThrows(ValidatorException.class, () -> {
            repo.add(task);
        });

        String expectedMessage = "Title must be between 1 and 255 characters";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @Timeout(5)
    void testAddTaskTitleValidDateMinInvalid() {
        this.task = new Task("task", new GregorianCalendar(1999, Calendar.DECEMBER, 31).getTime());
        ValidatorException exception = assertThrows(ValidatorException.class, () -> {
            repo.add(task);
        });

        String expectedMessage = "Date must be from 01.01.2000 to 31.12.2025";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

}